@extends('templates.layout')

@section('content')

	<div class="container mt-3">
		{{ Form::open(['url'=>'reports', 'method' => 'post']) }}

<div class="row">
	<div class="form-group col col-lg-4">
		<select name="cpf" class="form-control" required>
	<option value="">Selecione um funcionario</option>
		@if(isset($employees))
			 @foreach($employees as $employee) 
			<option value="{{$employee->cpf}}">{{$employee->name}}</option>
			 @endforeach
 			@endif
		</select>
	</div>

<div class="form-group col col-lg-4">
<label for="dateStart">data Inicio</label>
			<input type="date" name="dateStart" id="dateStart" class="btn btn-primary"  required>
</div>

<div class="form-group col col-lg-4">
					<label>data Final</label>
			<input type="date" name="dateEnd" class="btn btn-primary">
	<input type="submit" name="" class="btn btn-primary" required>
</div>
</div>		
	{{ Form::close() }}
	


@if(isset($points))
<table class="table">
		<thead>
			<tr>
				<th>Data</th>
				<th>Entrada</th>
				<th>Saída</th>
				<th>Entrada</th>
				<th>Saída</th>
			</tr>
	</thead>
	<tbody>
	@foreach($dateRange as $range)

	<tr>
		<td> {{ $range }}</td>
		@foreach($points as $point) 
		<?php 
		echo $point->date === $range ?  "<td>".$point->start_time ."</td>  <td>". $point->exit_time . "</td>" : '' ;
		?>
		@endforeach
	</tr>
	@endforeach
	</tbody>
	</table>
 @endif
	</div>

@endsection