@extends('templates.layout')

@section('content')

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js">
	
</script>

<script type="text/javascript">
	
$(document).on('click', '.browse', function(){
  var file = $(this).parent().parent().parent().find('.file');
  file.trigger('click');
});
$(document).on('change', '.file', function(){
  $(this).parent().find('.form-control').val($(this).val().replace(/C:\\fakepath\\/i, ''));
});
	
</script>

<style type="text/css">
	.file {
  visibility: hidden;
  position: absolute;
}
</style>

{{ Form::model($employee, ['route' => ['employees.update', $employee->id], 'method' => 'put']) }}
 
 <h1>Cadastro de Funcionarios</h1>
 <p>preencha todos os campos</p>
<div class="container">
	
  <div class="input-group mb-3">
   	 <span class="input-group-addon">
        <input type="checkbox" name="status" {{ $employee->status=='on' ? 'checked="true"' : ''  }}>
      </span>
      <span class="input-group-addon" >Ativo</span>
  	@if($errors->has('status')) 
  	<p class="help-block">{{ $errors->first('status') }}</p>
  	@endif
  </div>

  <div class="row">
  	<div class="form-group col-xs-12 col-md-6 col-lg-6">
      <input type="file" name="img[]" class="file">
      <div class="input-group col-xs-12">
        <span class="input-group-addon"><i class="glyphicon glyphicon-picture"></i></span>
        <input type="text" class="form-control input-lg" disabled placeholder="Imagem funcionário">
        <span class="input-group-btn">
          <button class="browse btn btn-primary input-lg" type="button"><i class="glyphicon glyphicon-search"></i> Buscar</button>
        </span>
      </div>
    </div>
  
  <div class="form-group col-xs-12 col-md-6 col-lg-6">
   	<input id="cpf" type="text" name="cpf" placeholder="CPF" class="form-control" value="{{ $employee->cpf or old('cpf') }}">
  	@if($errors->has('cpf')) 
  	<p class="help-block">{{ $errors->first('cpf') }}</p>
  	@endif
  </div>
</div>

<div class="row">
	<div class="form-group col-xs-12 col-md-6 col-lg-6">
	<label for="name">Nome</label>
	<input id="name" type="text" name="name" class="form-control" value="{{ $employee->name or old('name') }}">
		@if($errors->has('name')) 
	<p class="help-block">{{ $errors->first('name') }}</p>
	@endif
</div>

<div class="form-group col-xs-12 col-md-6 col-lg-6">
	<label for="function">Função</label>
	<input id="function" type="text" name="function" class="form-control" value="{{ $employee->function or old('function') }}">
	@if($errors->has('function')) 
	<p class="help-block">{{ $errors->first('function') }}</p>
	@endif
</div>
</div>

<div class="row">
	<div class="form-group  col-xs-12 col-md-6 col-lg-6">
	<label for="admission">Data de admissão</label>
	<input id="admission" type="text" name="admission" class="form-control" value="{{ $employee->admission or old('admission') }}">
		@if($errors->has('admission')) 
	<p class="help-block">{{ $errors->first('admission') }}</p>
	@endif
</div>
</div>


	<input type="submit" value="Enviar" class="btn btn-primary">

</div>

 {{ Form::close() }}


@endsection