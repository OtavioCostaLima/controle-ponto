@extends('templates.layout')

@section('content')

@if(isset($workloads) && isset($employee))
	<div class="container mt-4">

	<div class="row justify-content-start">
		<div class="form-group col col-lg-12">
			<h2>Colaborador: {{ $employee->name }}</h2><hr>
		</div>
	</div>

{{ Form::open([ 'route' => ['employee.workloads', $employee->id] ,'method' => 'POST']) }}
<div class="row">
  <div class="col-4">
    <div class="list-group" id="list-tab" role="tablist">
      @foreach($days as $day)
      <a class="list-group-item list-group-item-action {{ $day['key'] == 'segunda' ? 'active' : '' }}" id="list-{{$day['key']}}-list" data-toggle="list" href="#{{$day['key']}}" role="tab" aria-controls="home">{{$day['value']}}</a>
       @endforeach
    </div>
  </div>

  <div class="col-8">
    <div class="tab-content" id="nav-tabContent">
            @if(isset($workloads))
            @foreach($days as $day)
        <div class="tab-pane fade show {{ $day['key'] == 'segunda' ? 'active' : '' }}" id="{{$day['key']}}" role="tabpanel" aria-labelledby="list-{{$day['key']}}-list">
      	<table class="table table-bordered">
      		<thead>
      			<tr>
      				<th>selecionar</th>
              <th>Entrada/Saída</th>
      			</tr>
      		</thead>
      		<tbody>
      			@foreach($workloads as $workload)
      			@if($workload->day===$day['value'])
						<tr>
							<td>
                <input id="{{$workload->id}}" type="checkbox" name="day[]}}" value="{{$workload->id}}"
                  @foreach($employee->workloads as $value)
                  @if($workload->id === $value->id) checked 
                  @endif 
                  @endforeach>
              </td>
							<td>
								{{$workload->start_time. '-'. $workload->exit_time}}
							</td>
						</tr>
					 @endif
      		 @endforeach
      		</tbody>
      	</table>
         </div>
    @endforeach
    @endif
  </div>
</div> <!-- Fim da coluna -->
<hr>
</div> <!-- Fim da row -->

<div class="row justify-content-end">
  <input type="submit" value="Enviar" class="btn btn-primary" style="cursor: pointer;">
</div>
{{ Form::close() }}
</div> <!-- Fim do container -->
     @endif
@endsection