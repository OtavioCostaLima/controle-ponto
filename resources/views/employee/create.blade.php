@extends('templates.layout')

@section('content')

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js">
	
</script>
<script type="text/javascript">
	
$(document).on('click', '.browse', function(){
  var file = $(this).parent().parent().parent().find('.file');
  file.trigger('click');
});
$(document).on('change', '.file', function(){
  $(this).parent().find('.form-control').val($(this).val().replace(/C:\\fakepath\\/i, ''));
});
	
</script>

<style type="text/css">
	.file {
  visibility: hidden;
  position: absolute;
}
</style>

{{ Form::open(['url' => 'employees', 'method' => 'POST', 'enctype' => 'multipart/form-data']) }}
 
 <h1>Cadastro de Funcionarios</h1>
 <p>preencha todos os campos</p>
<div class="container">
	
  <div class="input-group mb-3">
   	 <span class="input-group-addon">
        <input type="checkbox" name="status">
      </span>
      <span class="input-group-addon " >Ativo</span>
  </div>

  <div class="row">
  	<div class="form-group col-xs-12 col-md-6 col-lg-6">
      <input type="file" name="img[]" class="file">
      <div class="input-group col-xs-12">
        <span class="input-group-addon"><i class="glyphicon glyphicon-picture"></i></span>
        <input type="text" class="form-control input-lg" disabled placeholder="Imagem funcionário">
        <span class="input-group-btn">
          <button class="browse btn btn-primary input-lg" type="button"><i class="glyphicon glyphicon-search"></i> Buscar</button>
        </span>
      </div>
    </div>
  
  <div class="form-group col-xs-12 col-md-6 col-lg-6">
   	<input id="cpf" type="text" name="cpf" placeholder="CPF" class="form-control {{ $errors->has('cpf')? 'is-invalid':'' }}" value="{{ old('cpf') }}">
  	@if($errors->has('cpf')) 
  	<p class="invalid-feedback">{{ $errors->first('cpf') }}</p>
  	@endif
  </div>
</div>

<div class="row">
	<div class="form-group col-xs-12 col-md-6 col-lg-6">
	<label for="name">Nome</label>
	<input id="name" type="text" name="name" class="form-control {{ $errors->has('name')? 'is-invalid':'' }}" value="{{ old('name') }}">
		@if($errors->has('name')) 
	<p class="invalid-feedback">{{ $errors->first('name') }}</p>
	@endif
</div>

<div class="form-group col-xs-12 col-md-6 col-lg-6">
	<label for="function">Função</label>
	<input id="function" type="text" name="function" class="form-control {{ $errors->has('function')? 'is-invalid':'' }}" value="{{ old('function') }}">
	@if($errors->has('function')) 
  <p class="invalid-feedback">{{ $errors->first('function') }}</p>
	@endif
</div>
</div>
<div class="row">
	<div class="form-group  col-xs-12 col-md-6 col-lg-6">
	<label for="admission">Data de admissão</label>
	<input id="admission" type="text" name="admission" class="form-control {{ $errors->has('admission')? 'is-invalid':'' }}" value="{{ old('admission') }}">
		@if($errors->has('admission')) 
	<p class="invalid-feedback">{{ $errors->first('admission') }}</p>
	@endif
</div>
</div>


	<input type="submit" value="Enviar" class="btn btn-primary">

</div>

 {{ Form::close() }}


@endsection