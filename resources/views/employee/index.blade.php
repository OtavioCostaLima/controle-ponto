@extends('templates.layout')

@section('content')

<div class="container mt-4">

 <div class="row justify-content-end">
		<div class="form-group col col-lg-1">
			<a href="{{ route('employees.create') }}" class="btn btn-primary">novo</a>
		</div>
	</div>

	@if(isset($employees))
<table class="table">
	<thead>
		<tr class="thead-default">
			<th>cpf</th>
			<th>Nome</th>
			<th>Ações</th>
		</tr>
	</thead>
	<tbody>
		@foreach($employees as $employee)
		<tr>
			<td>{{ $employee->cpf }}</td>
			<td>{{ $employee->name }}</td>
			<td align="center">
			<a href="{{ url('employees/'. $employee->id. '/workloads/' ) }}" class="text-success"><i class="fa fa-calendar fa-2x" aria-hidden="true"></i></a>
			<a href="{{ route('employees.edit', ['id'=>$employee->id]) }}"><i class="fa fa-pencil-square-o fa-2x" aria-hidden="true"></i></a>

			<a href="{{ url('employees/' . $employee->id) . '/status'  }}" class="{{ $employee->status=='on' ? 'text-success': 'text-danger'   }}"><i class="fa {{ $employee->status=='on' ? 'fa-unlock-alt': 'fa-lock' }} fa-2x" aria-hidden="true"></i></a></td>
		</tr>
		@endforeach
	</tbody>
</table>
@endif

 	<div class="row justify-content-center">{!! $employees->render("pagination::bootstrap-4") !!}</div>

</div>
@endsection