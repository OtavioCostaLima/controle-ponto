@extends('templates.layout')

@section('content')

{{ Form::open(['route' => 'points.store' , 'method' => 'post']) }}



 <h1>Cadastro de Ponto</h1>
 <p>preencha todos os campos</p>
<div class="container">
	

<div class="">
	@if(isset($errors->message) && !is_null($errors->message)) 
	<h2>{{ $errors->message }}</h2>
	@endif
</div>
	
<div class="row">
	
<div>
	<label>Hora extra</label>
	<input type="checkbox" name="">
</div>

<div class="form-group col col-lg-6">
	<input id="cpf" type="text" name="cpf" placeholder="cpf" class="form-control" required>
</div>

<div class="form-group col col-lg-4">
	<input type="submit" value="Enviar" class="btn btn-primary">
</div>

</div>


	@if(isset($point)) 
	<div class="alert alert-success" role="alert">
<button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
	<b>Funcionário:</b> {{ $employee->name }} -
		<b>Data/Hora:</b> {{ $point->date }} 
		{{ is_null($point->exit_time) ? $point->start_time : $point->exit_time }} 
		- <b>Tipo:</b> {{ $type }}
	</div>
	@else 

	@endif


{{ Form::close() }}

@endsection