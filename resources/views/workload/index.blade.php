@extends('templates.layout')

@section('content')


	<div class="container mt-4">
	<div class="row justify-content-end">
		<div class="form-group col col-lg-1">
			<a href="{{ route('workloads.create') }}" class="btn btn-primary">novo</a>
		</div>
	</div>


<div class="row">
  <div class="col-4">
    <div class="list-group" id="list-tab" role="tablist">
      @foreach($days as $day)
      <a class="list-group-item list-group-item-action {{ $day['key'] == 'segunda' ? 'active' : '' }}" id="list-{{$day['key']}}-list" data-toggle="list" href="#{{$day['key']}}" role="tab" aria-controls="home">{{$day['value']}}</a>
       @endforeach
    </div>
  </div>
  <div class="col-8">
    <div class="tab-content" id="nav-tabContent">
            @if(isset($workloads))
            @foreach($days as $day)
      <div class="tab-pane fade show {{ $day['key'] == 'segunda' ? 'active' : '' }}" id="{{$day['key']}}" role="tabpanel" aria-labelledby="list-{{$day['key']}}-list">
      	@if(isset($workloads))
      	<table class="table table-bordered">
      		<thead>
      			<tr>
      				<th>Entrada/Saída</th>
      				<th>Ação</th>
      			</tr>
      		</thead>
      		<tbody>
      			@foreach($workloads as $workload)
      				@if($workload->day==$day['value'])
						<tr>
							<td>
								{{$workload->start_time. '-'. $workload->exit_time}}
							</td>
							<td>

{!! Form::open(['method' => 'Delete', 'route' => ['workloads.destroy', $workload->id]]) !!}
<button type="submit" class="btn btn-danger" style="cursor: pointer;">
  <i class="fa fa-times" aria-hidden="true"></i>
</button>
{!! Form::close() !!}
</td>
						</tr>
					@endif
      			 @endforeach
      		</tbody>
      	</table>
           @endif  
 
       </div>
             @endforeach
    @endif 
     </div>
   </div>
 </div>
</div>

@endsection