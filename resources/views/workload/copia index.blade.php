@extends('templates.layout')

@section('content')


	<div class="container mt-4">
	<div class="row justify-content-end">
		<div class="form-group col col-lg-1">
			<a href="{{ route('workloads.create') }}" class="btn btn-primary">novo</a>
		</div>
	</div>


<div class="row">
  <div class="col-4">
    <div class="list-group" id="list-tab" role="tablist">
      <a class="list-group-item list-group-item-action active" id="list-segunda-list" data-toggle="list" href="#segunda" role="tab" aria-controls="home">Segunda</a>
      <a class="list-group-item list-group-item-action" id="list-terca-list" data-toggle="list" href="#terca" role="tab" aria-controls="profile">Terça</a>
      <a class="list-group-item list-group-item-action" id="list-quarta-list" data-toggle="list" href="#quarta" role="tab" aria-controls="messages">Quarta</a>
      <a class="list-group-item list-group-item-action" id="list-quinta-list" data-toggle="list" href="#quinta" role="tab" aria-controls="settings">Quinta</a>
      <a class="list-group-item list-group-item-action" id="list-sexta-list" data-toggle="list" href="#sexta" role="tab" aria-controls="settings">Sexta</a>
      <a class="list-group-item list-group-item-action" id="list-sabado-list" data-toggle="list" href="#sabado" role="tab" aria-controls="settings">Sabado</a>
      <a class="list-group-item list-group-item-action" id="list-domingo-list" data-toggle="list" href="#domingo" role="tab" aria-controls="settings">Domingo</a>
    </div>
  </div>
  <div class="col-8">
    <div class="tab-content" id="nav-tabContent">
      <div class="tab-pane fade show active" id="segunda" role="tabpanel" aria-labelledby="list-segunda-list">
      	@if(isset($workloads))
      	<table class="table table-bordered">
      		<thead>
      			<tr>
      				<th>Entrada/Saída</th>
      				<th>Ação</th>
      			</tr>
      		</thead>
      		<tbody>
      			@foreach($workloads as $workload)
      				@if($workload->day==='Segunda')
						<tr>
							<td>
								{{$workload->start_time. '-'. $workload->exit_time}}
							</td>
							<td><a href="">deletar</a></td>
						</tr>
					@endif
      			 @endforeach
      		</tbody>
      	</table>
      	@endif
      </div>
      <div class="tab-pane fade" id="terca" role="tabpanel" aria-labelledby="list-terca-list">
      	       	@if(isset($workloads))
      	<table class="table table-bordered">
      		<thead>
      			<tr>
      				<th>Entrada/Saída</th>
      				<th>Ação</th>
      			</tr>
      		</thead>
      		<tbody>
      			@foreach($workloads as $workload)
      				@if($workload->day==='Terça')
						<tr>
							<td>
								{{$workload->start_time. '-'. $workload->exit_time}}
							</td>
							<td><a href="">deletar</a></td>
						</tr>
					@endif
      			 @endforeach
      		</tbody>
      	</table>
      	@endif
      </div>
      <div class="tab-pane fade" id="quarta" role="tabpanel" aria-labelledby="list-quarta-list">
                        @if(isset($workloads))
            <table class="table table-bordered">
                  <thead>
                        <tr>
                              <th>Entrada/Saída</th>
                              <th>Ação</th>
                        </tr>
                  </thead>
                  <tbody>
                        @foreach($workloads as $workload)
                              @if($workload->day==='Quarta')
                                    <tr>
                                          <td>
                                                {{$workload->start_time. '-'. $workload->exit_time}}
                                          </td>
                                          <td><a href="">deletar</a></td>
                                    </tr>
                              @endif
                         @endforeach
                  </tbody>
            </table>
            @endif
      </div>
      <div class="tab-pane fade" id="quinta" role="tabpanel" aria-labelledby="list-settings-list">
      	      	@if(isset($workloads))
      	<table class="table table-bordered">
      		<thead>
      			<tr>
      				<th>Entrada/Saída</th>
      				<th>Ação</th>
      			</tr>
      		</thead>
      		<tbody>
      			@foreach($workloads as $workload)
      				@if($workload->day==='Quinta')
						<tr>
							<td>
								{{$workload->start_time. '-'. $workload->exit_time}}
							</td>
							<td><a href="">deletar</a></td>
						</tr>
					@endif
      			 @endforeach
      		</tbody>
      	</table>
      	@endif
      </div>
      <div class="tab-pane fade" id="sexta" role="tabpanel" aria-labelledby="list-messages-list">
      	      	@if(isset($workloads))
      	<table class="table table-bordered">
      		<thead>
      			<tr>
      				<th>Entrada/Saída</th>
      				<th>Ação</th>
      			</tr>
      		</thead>
      		<tbody>
      			@foreach($workloads as $workload)
      				@if($workload->day==='Sexta')
						<tr>
							<td>
								{{$workload->start_time. '-'. $workload->exit_time}}
							</td>
							<td><a href="">deletar</a></td>
						</tr>
					@endif
      			 @endforeach
      		</tbody>
      	</table>
      	@endif
      </div>
      <div class="tab-pane fade" id="sabado" role="tabpanel" aria-labelledby="list-settings-list">
      	      	@if(isset($workloads))
      	<table class="table table-bordered">
      		<thead>
      			<tr> 
      				<th>Entrada/Saída</th>
      				<th>Ação</th>
      			</tr>
      		</thead>
      		<tbody>
      			@foreach($workloads as $workload)
      				@if($workload->day==='Sabado')
						<tr>
							<td>
								{{$workload->start_time. '-'. $workload->exit_time}}
							</td>
							<td><a href="">deletar</a></td>
						</tr>
					@endif
      			 @endforeach
      		</tbody>
      	</table>
      	@endif
      </div>

  <div class="tab-pane fade" id="domingo" role="tabpanel" aria-labelledby="list-domingo-list">
      	      	@if(isset($workloads))
      	<table class="table table-bordered">
      		<thead>
      			<tr>
      				<th>Entrada/Saída</th>
      				<th>Ação</th>
      			</tr>
      		</thead>
      		<tbody>
      			@foreach($workloads as $workload)
      				@if($workload->day==='Domingo')
						<tr>
							<td>
								{{$workload->start_time. '-'. $workload->exit_time}}
							</td>
							<td><a href="">deletar</a></td>
						</tr>
					@endif
      			 @endforeach
      		</tbody>
      	</table>
      	@endif
      </div>

    </div>
  </div>
</div>



	</div>

@endsection