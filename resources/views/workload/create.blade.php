@extends('templates.layout')

@section('content')

	{{ Form::open(['route' => 'workloads.store', 'method'=>'post']) }}
	

<div class="container mt-4">
	<div class="row">
		<div class="form-group col col-lg-3">
			<label for="cpf">Data Entrada</label>
			<select class="form-control" name="day">
				<option value="Segunda">Segunda</option>
				<option value="Terça">Terça</option>
				<option value="Quarta">Quarta</option>
				<option value="Quinta">Quinta</option>
				<option value="Sexta">Sexta</option>
				<option value="Sabado">Sabado</option>
				<option value="Domingo">Domingo</option>
			</select>
			@if($errors->has('cpf')) 
			<p class="help-block">{{ $errors->first('cpf') }}</p>
			@endif
		</div>

		<div class="form-group col col-lg-3">
			<label for="start_time">Hora Entrada</label>
			<input id="start_time" type="time" name="start_time" class="form-control">
			@if($errors->has('cpf')) 
			<p class="help-block">{{ $errors->first('cpf') }}</p>
			@endif
		</div>

	<div class="form-group col col-lg-3">
		<label for="exit_time">Hora Saída</label>
		<input id="exit_time" type="time" name="exit_time" class="form-control">
		@if($errors->has('cpf')) 
		<p class="help-block">{{ $errors->first('cpf') }}</p>
		@endif
	</div>

</div>

<div class="row">
		<div class="form-group col col-lg-3">
		<input type="submit" class="form-control btn btn-primary">
	</div>
</div>

</div>

	{{ Form::close() }}
@endsection