<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ReportController extends Controller
{

	function __construct()
	{

	}

	public function index() 
	{
		$title = "Controle";
		$employees = \App\Models\Employee::all();
   //		$points = \App\Models\Point::all();
		return view('report.index', compact('employees','title'));
	}


public function generateByEmployee(Request $request)
{
	$dateStart = $request->dateStart;
	$dateEnd = $request->dateEnd;
	$cpf = $request->cpf;

   $dR = $this->getIntervalDate($dateStart, $dateEnd);

	$employee = \App\Models\Employee::where('cpf', '=', $cpf)->first();
	$points = $employee->points->where('date','>=', $dateStart)->where('date','<=', $dateEnd);
	$dates = $employee->points()->select('date')->where('employee_id','=', $employee->id)->where('date','>=', $dateStart)->where('date','<=', $dateEnd)->get();
	    
	    $array = array();
        foreach ($dates as $date => $value) {
            array_push($array,$value->date);
        }

		$dateRange = array();
			for ($i = 0; $i < sizeof($dR); $i++) {
				$val[] = array_search($dR[$i], $array);
				if($val[$i]!==false){
		 		array_push($dateRange,$dR[$i]);
			}
		}

	$employees = \App\Models\Employee::all();	
	return view('report.index', compact('points', 'dateRange','employees'));
}

	private function getIntervalDate($dateStart, $dateEnd) {
		$dateStart = implode('-', array_reverse(explode('/', substr($dateStart, 0, 10)))).substr($dateStart, 10);
    	$dateStart = new \DateTime($dateStart);

		$dateEnd = implode('-', array_reverse(explode('/', substr($dateEnd, 0, 10)))).substr($dateEnd, 10);
    	$dateEnd = new \DateTime($dateEnd);

   		$dateRange = array();

    	while($dateStart <= $dateEnd){
        	$dateRange[] = $dateStart->format('Y-m-d');
        	$dateStart = $dateStart->modify('+1day');
    	}
		return $dateRange;
	}

}