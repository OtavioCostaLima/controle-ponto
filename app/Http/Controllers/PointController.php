<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Point;

class PointController extends Controller
{
	private $point;

    function __construct(Point $point)
    {
    	$this->point = $point;
    }


    public function index() 
    {
    	$points = $this->point->all();	
        return view('point.index',compact('points'));
    }

    public function create() {
    	$title =  'Controle de Ponto';
    	return view('Point.create', compact('title'));
    }

    public function store(Request $request) {
    	$title =  'Controle de Ponto';
    	$cpf = $request->cpf;
	 	date_default_timezone_set('America/Sao_Paulo');
	 	$employee = \App\Models\Employee::where('cpf', '=', $cpf)->first();

	 	if(!$employee) { 
	 		$errors = ['message' => 'Funcionário não encontrado!'];
	 	  	return view('Point.create', compact('errors'));
    	}

	 	// busca os ponto  do funcionario que entrou hoje e ainda não saiu
	    $point = $this->point->whereIn('employee_id', [$employee->id])->whereDate('date', date('d/m/Y'))->whereNull('exit_time')->first();

  	    //se $point for nulo deve registrar a entrada do funcionário
    	 if (is_null($point)) {
            //cadastra a entrada do funcionario
            $point = $this->point->create([
            'date' => date('d/m/Y'),
            'start_time' => date('H:i'),
            'employee_id' => $employee->id
            ]);
            $type = "ENTRADA";
    	 } else {
           // registrar saida
            $point->exit_time = date(' H:i');
            $point->save();
            $type = "SAÍDA";
    	}

	return view('Point.create', compact('point', 'employee', 'type','title'));
   }
}
