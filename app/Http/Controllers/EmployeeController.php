<?php

namespace App\Http\Controllers;

use App\Http\Requests\EmployeeFormRequest;
use Illuminate\Http\Request;
use App\Models\Employee;
class EmployeeController extends Controller
{
	private $employee;

	function __construct(Employee $employee)
	{
		$this->employee = $employee;
	}

     public function index()
    {
      $title = "Colaboradores";
      $employees = $this->employee->orderBy('id')->paginate(4);
    	return view('employee.index', compact('employees','title'));
    }

    public function edit($id) 
    {
      $title = "Editar Colaborador";
      $employee = $this->employee->find($id);
     return view('employee.edit',compact('employee','title'));
    }

    public function update(EmployeeFormRequest $request, $id)
    {
      $employee = $this->employee->find($id);
      $employee->update($request->all());
      return redirect()->route('employees.index');
    }

    public function store(EmployeeFormRequest $request)
    {        
            $request['status'] = $request->all()['status'] ?? 'off';
            $data = $request->all();
            $this->employee->create($data);
    	return redirect()->route('employees.index');
    }

    public function create()
    {
       $title = "Novo colaborador";
     	return view('employee.create', compact('title'));
    }

    public function show($id) {

    }

    public function alterStatus($id)
    {
       $employee = $this->employee->find($id);
       $status = $employee->status == 'on' ? 'off' : 'on';
       $employee->status = $status;
       $employee->save();
       return redirect()->route('employees.index');
    }

     public function destroy($id) {


     }

     public function createWorkload($id, Request $request)
     {
       $employee = $this->employee->find($id);
        if(isset($request->day)) {
        $workload =  \App\Models\Workload::whereIn('id', $request->day)->get();
        $employee->workloads()->sync($workload);
        }
     
      return $this->workload($employee->id);
     }

    public function workload($id) {
      $title = "Expediente colaborador";
      $days = array(
        ['key'=>'segunda', 'value' => 'Segunda'],
        ['key'=>'terca', 'value' => 'Terca'],
        ['key'=>'quarta', 'value' => 'Quarta'],
        ['key'=>'quinta', 'value' => 'Quinta'],
        ['key'=>'sexta', 'value' => 'Sexta'],
        ['key'=>'sabado', 'value' => 'Sabado'],
        ['key'=>'domingo', 'value' => 'Domingo']);

      $workloads = \App\Models\Workload::orderBy('start_time', 'asc')->get();
      $employee = $this->employee->find($id);
      $employee->workloads;
      return view('employee.workload', compact('workloads','employee','title', 'days'));
    }


}
