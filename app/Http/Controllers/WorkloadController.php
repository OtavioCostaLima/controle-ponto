<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Workload;


class WorkloadController extends Controller
{

	private $workload;

    function __construct(Workload $workload)
    {
    	$this->workload = $workload;
    }

    public function index()
    {
        $days = array(
        ['key'=>'segunda', 'value' => 'Segunda'],
        ['key'=>'terca', 'value' => 'Terca'],
        ['key'=>'quarta', 'value' => 'Quarta'],
        ['key'=>'quinta', 'value' => 'Quinta'],
        ['key'=>'sexta', 'value' => 'Sexta'],
        ['key'=>'sabado', 'value' => 'Sabado'],
        ['key'=>'domingo', 'value' => 'Domingo']);

    	$workloads = $this->workload->orderBy('start_time', 'asc')->get();
        return view('workload.index', compact('workloads', 'days')) ;
    }

    public function create()
    {
    	$title = "Cadatrar espediente";
    	return view('workload.create', compact('title'));
    }


    public function store(Request $request)
    {
           $request->validate([
            'day' => 'required',
            'start_time' => 'required',
            'exit_time' => 'required'
    ]);
                    $this->workload->create($request->all());
    	return redirect()->route('workloads.index');
    }

    public function destroy($id)
    {
         $workload = $this->workload->find($id);
         $workload->delete();
         return redirect()->route('workloads.index');
    }

}
