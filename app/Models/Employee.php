<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
	protected $fillable = ['cpf', 'name', 'function', 'admission','status'];

	public function points() {
		return $this->hasMany(\App\Models\Point::class,'employee_id', 'id');
	}

		public function workloads()
	{
		return $this->belongsToMany(\App\Models\Workload::class,'employee_workload');
	}

}
