<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Point extends Model
{
	public $timestamps = false;

	protected $fillable = ['employee_id', 'date', 'start_time', 'exit_time'];

    public function employee() 
    {
    	return $this->belongsTo(\App\Models\Employee::class);
    }
}
