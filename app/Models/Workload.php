<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Workload extends Model
{
	protected $fillable = ['day', 'start_time', 'exit_time'];


	public function employees()
	{
		return $this->belongsToMany('employee_workload');
	}
}
