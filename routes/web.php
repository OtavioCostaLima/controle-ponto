<?php

Route::resource('employees', 'EmployeeController');
Route::resource('points', 'PointController');
Route::resource('workloads', 'WorkloadController');
Route::get('reports', 'ReportController@index')->name('reports.index');
Route::post('reports', 'ReportController@generateByEmployee');

Route::post('employees/{id}/workloads', 'EmployeeController@createWorkload')->name('employee.workloads');
Route::get('employees/{id}/workloads', 'EmployeeController@workload')->name('employee.workloads');



Route::get('employees/{id}/status', 'EmployeeController@alterStatus');